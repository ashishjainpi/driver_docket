import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgForm} from '@angular/forms';

// import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { TableHeaderFixerComponent } from './table-header-fixer/table-header-fixer.component';
import * as $ from 'jquery';
import { ReactiveFormsModule } from '@angular/forms';  // <-- #1 import module
import { NgVirtualKeyboardModule }  from '@protacon/ng-virtual-keyboard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    TableHeaderFixerComponent,
  ],
  imports: [
    NgVirtualKeyboardModule,
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
