import { Component } from '@angular/core';
// import { HttpClient,HttpParams } from '@angular/common/http';
import { Http, Response, Headers } from '@angular/http';
// import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
// import { Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  size;
  data;
  data2;
  _id;
  onesecond=60000;
  orderplace=[];
  index_in_orderplace;
  form:FormGroup;
  time_var;
  selectrow=0;
  loged_in_drivers=[];
  assigned_order_in_driver_table=[];
  orderbundle_arr_to_udate=[]
  all_drivers;
  intervaldata; 
  totalorders=0;
  stores;
  constructor(private http: Http,private fb: FormBuilder) {
    this.selected_store.name = localStorage.getItem('selected_store_name');
    this.get('http://13.56.147.24:9030/all-outlet/nokey').map((res: Response) => res.json()).subscribe(outlet => {
          this.stores=outlet;
      });    
    
      this.form = this.fb.group({
        emp_id: [null, [Validators.required, Validators.email]],
        employee: [null, [Validators.required]]
      });
      Observable.interval(250 * 60).subscribe(x => {
        console.log('interval')
        this.get('http://13.56.147.24:9030/admin-new-order-with-schedule/nokey').map((res: Response) => res.json()).subscribe(data => {
        
            this.intervaldata = data;
            
            console.log(this.intervaldata.length,this.totalorders);
            // if (this.intervaldata && this.intervaldata.length < this.totalorders) {
              // console.log('this.data',this.data);
              for (let k = 0;k < this.data.length; k++) {
                let index = this.intervaldata.findIndex(x => x.auto_id==this.data[k].auto_id);
                // console.log('index in interval',index);
                if(index==-1)
                {
                  this.data.splice(k,1);
                  this.totalorders--;
                }else{
                  // console.log('found');
                } 
              }
            // }
            console.log(this.intervaldata.length,this.totalorders);
            if (this.intervaldata && this.intervaldata.length > this.totalorders) {
              console.log('yes');
              // let difference = this.intervaldata.length-this.totalorders;
              // this.totalorders+=difference;
              
              // for (let k = 0;k < difference; k++) {
              //     this.setcolor(this.intervaldata[k]);
              //     // this.data.push(this.intervaldata[k]);
              //     this.data.unshift(this.intervaldata[k]);
              //     // this.data(this.intervaldata[k],'shift');  
              //  }  
              
              for (let k = 0;k < this.intervaldata.length; k++) {
                // console.log('k',k);
                // console.log('data',data[k]);
                  // this.orderplace.push(data[k]);
                  let index_in_all_orders = this.data.findIndex(x=>x.auto_id==this.intervaldata[k].auto_id);
                  
                  console.log(index_in_all_orders);
  
                  if(index_in_all_orders==-1 && (this.intervaldata[k].driverauto_id==undefined || this.intervaldata[k].driver_auto_id==-1)){
                    console.log('in push');
                    this.setcolor(this.intervaldata[k]);
                  // this.data.push(this.intervaldata[k]);
                    this.data.unshift(this.intervaldata[k]);
                }
                   // this.ordercheckfunction(data[k],'shift');           
               }
  
            }
  
            /*order-item*/
          });
  
          let url = 'http://13.56.147.24:9030/all-drivers/nokey';
      this.get(url).map((res: Response) => res.json()).subscribe(data => {
         this.all_drivers=data;
        for(let k=0; k<(<any>data).length; k++)
       {  data[k].emp=false;
         let index = this.loged_in_drivers.findIndex(x=>x.emp_auto_id==data[k].auto_id && x.emp!=true)
          
          if(index!=-1 && !data[k].emp)
          {
            this.loged_in_drivers[index].assigned_order=data[k].assigned_order;
          }  
  
        //  for(let l=0; l<data[k].assigned_order.length; l++)
        // {
        //   // console.log('driver-->',data[k].assigned_order[l]);
        //   this.assigned_order_in_driver_table.push(data[k].assigned_order[l]);
        // }
  
       }
       sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
      //  console.log(this.assigned_order_in_driver_table);
  
      //employee login
      url = 'http://13.56.147.24:9030/all-employee/nokey';
      this.get(url).map((res: Response) => res.json()).subscribe(data => {
        //  this.all_drivers.push(data);
        for(let k=0; k<(<any>data).length; k++)
       {
        data[k]['emp']=true;
         this.all_drivers.push(data[k]);
         let index = this.loged_in_drivers.findIndex(x=>x.emp_auto_id==data[k].auto_id && x.emp==true)
          
          if(index!=-1 && data[k].emp)
          {
            this.loged_in_drivers[index].assigned_order=data[k].assigned_order;
          }  
          for(let k=0; k<this.all_drivers.length; k++)
       {
           for(let l=0; l<this.all_drivers[k].assigned_order.length; l++)
          {
            // console.log('driver-->',data[k].assigned_order[l]);
            this.assigned_order_in_driver_table.push(this.all_drivers[k].assigned_order[l]);
          }
       }
        //  for(let l=0; l<data[k].assigned_order.length; l++)
        // {
        //   // console.log('driver-->',data[k].assigned_order[l]);
        //   this.assigned_order_in_driver_table.push(data[k].assigned_order[l]);
        // }
  
       }
       console.log('this.all_drivers',this.all_drivers);
       sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
      //  console.log(this.assigned_order_in_driver_table);
      });
  
      //employee login
  
      });
  
      
  
      }); //interval end
  
      // let url = 'http://13.56.147.24:9030/all-drivers/nokey';
      // this.get(url).subscribe(data => {
      //    this.all_drivers=data;
        
  
       //emp
      //  let url = 'http://13.56.147.24:9030/all-employee/nokey';
      // this.get(url).subscribe(data => {
      //    this.all_drivers=data;
      //   for(let k=0; k<(<any>data).length; k++)
      //  {
      //      for(let l=0; l<data[k].assigned_order.length; l++)
      //     {
      //       // console.log('driver-->',data[k].assigned_order[l]);
      //       this.assigned_order_in_driver_table.push(data[k].assigned_order[l]);
      //     }
      //  }
       //emp
      //  console.log(this.assigned_order_in_driver_table);
      // });
      console.log('all-driver',this.all_drivers);
      console.log("sessionStorage.getItem('loged_in_drivers')",sessionStorage.getItem('loged_in_drivers'));
      if(sessionStorage.getItem('loged_in_drivers')!=null && sessionStorage.getItem('loged_in_drivers')!=undefined && sessionStorage.getItem('loged_in_drivers')!='')
      {
        this.loged_in_drivers = JSON.parse(sessionStorage.getItem('loged_in_drivers'));
      }
  
  // http://13.56.147.24:9030/admin-new-order-with-schedule/nokey
  
      this.get(`http://13.56.147.24:9030/admin-new-order-with-schedule/nokey`).map((res: Response) => res.json()).subscribe(data => {
        this.data=data;
        this.totalorders=this.data.length;
        for(let k=0; k<this.data.length; k++)
       {
         this.data[k].total = (parseFloat(this.data[k].total)).toFixed(2);
         this.setcolor(this.data[k]);
       }
    });  
  }

  selected_store={name:''};
  createAuthorizationHeader(headers: Headers) {
    if(this.selected_store.name!='')
    {
      headers.append('store', this.selected_store.name); 
    }
  }

  get(url) {
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(url, {
      headers: headers
    });
  }

  afterselect_store(i)
  {
    this.selected_store = this.stores[i];
    localStorage.setItem('selected_store_name',this.selected_store.name);
    window.location.reload();
  }


checkinresponse;
// checkinres;
checkout;
emptocheckout;
checkin(e,checkout)
   {     
    let emp_id = e.value.emp_id;
     emp_id = emp_id.split('000');
     emp_id = emp_id[1]
     console.log(checkout,emp_id);
     if(emp_id!=undefined)
     {
    let starttime=Date.now()
    this.get('http://13.56.147.24:9030/getemployee_by_autoid/'+emp_id+'/nokey').map((res: Response) => res.json()).subscribe(empdata=>{
      if((<any>empdata).length>0)
      {
       console.log('http://13.56.147.24:9030/get_by_emp_id_checkin_status/'+empdata[0]._id+'/nokey');
       
       this.get('http://13.56.147.24:9030/get_by_emp_id_checkin_status/'+empdata[0]._id+'/nokey').map((res: Response) => res.json()).subscribe(checkdata=>{
          console.log("(<any>checkdata).length",(<any>checkdata).length);
          if((<any>checkdata).length>0 && checkout==false)
          {
            this.checkout=true;
            this.emptocheckout={'id':emp_id};
          }else if(checkout==false){
            console.log('http://13.56.147.24:9030/create-emp_checkin/nokey');
            this.http.post('http://13.56.147.24:9030/create-emp_checkin/nokey', {'emp_id':empdata[0]._id,'emp_name':empdata[0].name,'emp_auto_id':empdata[0].auto_id,'login_time':starttime}).map((res: Response) => res.json()).subscribe(data=>{
                console.log('create-emp_checkin',data);
                var checkinvari = document.getElementById('checkinres');
          checkinvari.innerText="CheckIn Successfully";
          checkinvari.classList.add("alert");
        checkinvari.classList.add("alert-success");  

                setTimeout(function(){ 
                  var checkinvari = document.getElementById('checkinres');
          checkinvari.innerText="";
          checkinvari.classList.remove("alert");
        checkinvari.classList.remove("alert-success");  
                  document.getElementById('closebutton_emp').click();
                  
                 },2000);
                 e.reset();
                // this.checkinresponse='';
                // document.getElementById('closebutton').click();
            });
          }
        

          if(checkout)
          {
            console.log(this.emptocheckout.id,"==",empdata[0].auto_id);
            if(this.emptocheckout.id==empdata[0].auto_id)
            {
            console.log({'emp_name':empdata[0].name,'emp_id':empdata[0]._id,'emp_auto_id':empdata[0].auto_id,'logout_time':starttime,'total_earning':checkdata[0].total_earning,'login_time':checkdata[0].login_time,'total_discount':checkdata[0].total_discount});

            console.log('http://13.56.147.24:9030/update_emp_checkout_reciept/' + checkdata[0]._id + '/nokey');

            this.http.put('http://13.56.147.24:9030/update_emp_checkout_reciept/' + checkdata[0]._id + '/nokey', 
            {'emp_name':empdata[0].name,'emp_id':empdata[0]._id,'emp_auto_id':empdata[0].auto_id,'logout_time':starttime,'total_earning':checkdata[0].total_earning,'login_time':checkdata[0].login_time,'total_discount':checkdata[0].total_discount}).map((res: Response) => res.json()).subscribe(data=>{
                console.log('logout-emp_session',data);      
                var checkinvari = document.getElementById('checkinres');
          checkinvari.innerText="CheckOut Successfully";
          checkinvari.classList.add("alert");
        checkinvari.classList.add("alert-success"); 
                setTimeout(function(){ 
                  
                  checkinvari.innerText="";
                  checkinvari.classList.remove("alert");
                checkinvari.classList.remove("alert-success");

                  document.getElementById('closebutton_emp').click();
                  // console.log('checkinresponse2',this.checkinresponse);
                 },2000);
                 e.reset();
                 this.checkout=false;
            });
          }else{
            var checkinvari = document.getElementById('checkinres');
          checkinvari.innerText="Employee ID Changed Login First";
          checkinvari.classList.add("alert");
          checkinvari.classList.add("alert-danger"); 
                setTimeout(function(){   
                  checkinvari.innerText="";
                  checkinvari.classList.remove("alert");
                  checkinvari.classList.remove("alert-danger");
                 },1000);
                 this.checkout=false;
          }
          }

          // console.log('checkinresponse1',this.checkinresponse);
            

        });
      }
  });
}else{
  var checkinvari = document.getElementById('checkinres');
          checkinvari.innerText="Please provide a valid employee ID";
          checkinvari.classList.add("alert");
          checkinvari.classList.add("alert-danger"); 
                setTimeout(function(){   
                  checkinvari.innerText="";
                  checkinvari.classList.remove("alert");
                  checkinvari.classList.remove("alert-danger");
                 },2000);
}
 }

 checkinemp(e)
   {
    //  console.log(this.checkinres.json());
    let emp_id = e.value.emp_id;
    let starttime=Date.now()
    this.get('http://13.56.147.24:9030/getemployee_by_autoid/'+emp_id+'/nokey').map((res: Response) => res.json()).subscribe(empdata=>{
      if((<any>empdata).length>0)
      {
       console.log('http://13.56.147.24:9030/checkin_print/nokey');
       this.http.post('http://13.56.147.24:9030/checkin_print/nokey',{'emp_id':empdata[0]._id,'emp_name':empdata[0].name,'emp_auto_id':empdata[0].auto_id,'login_time':starttime}).map((res: Response) => res.json()).subscribe(checkdata=>{
          var checkinvari = document.getElementById('checkinres');
          checkinvari.innerText="CheckIn Successfully";
          checkinvari.classList.add("alert");
        checkinvari.classList.add("alert-success");  
          e.reset();
            
                setTimeout(function(){ 
                  checkinvari.innerText="";
                  checkinvari.classList.remove("alert");
                  checkinvari.classList.remove("alert-success");  
                  document.getElementById('closebutton_emp').click();
                  
                },2000);
          
        });
      }
  });
 }

 checkoutemp(e)
 {
  let emp_id = e.value.emp_id;
  let starttime=Date.now()
  this.get('http://13.56.147.24:9030/getemployee_by_autoid/'+emp_id+'/nokey').map((res: Response) => res.json()).subscribe(empdata=>{
    if((<any>empdata).length>0)
    {
     console.log('http://13.56.147.24:9030/checkout_print/nokey');
     this.http.post('http://13.56.147.24:9030/checkout_print/nokey',{'emp_id':empdata[0]._id,'emp_name':empdata[0].name,'emp_auto_id':empdata[0].auto_id,'login_time':starttime}).map((res: Response) => res.json()).subscribe(checkdata=>{
        var checkoutvari = document.getElementById('checkoutres');
        checkoutvari.innerText="CheckOut Successfully";
        checkoutvari.classList.add("alert");
        checkoutvari.classList.add("alert-success");  
        e.reset();

        setTimeout(function(){ 
          document.getElementById('checkoutres').innerText="";
          checkoutvari.classList.remove("alert");
          checkoutvari.classList.remove("alert-success")
          document.getElementById('closebutton_emp_out').click();
         },2000);

      });
    }
});
}


setcolor(orderdata){
  setTimeout(function(){ orderdata.color='#faebcc'; },this.onesecond*5 );
    setTimeout(function(){ orderdata.color='#ebccd1';  },this.onesecond*7);
   // orderdata.color='red'; console.log(orderdata); }, 50000);
}

logout=false;
drivertologin;
drivertologin_type;
drivertologout;
employee;  
showDriver(f){
  let e = f.value;
  console.log(e);
  console.log(e.emp_id);
  if(e.employee==null)
  {
    e.employee=false;
  }
  if(e.emp_id==undefined)
  {
    f.reset();
  }
  // console.log(this.drivertologin_type, this.drivertologin_type!=e.employee);
  // console.log(this.drivertologin,e.emp_id,this.drivertologin_type);
  // if(e.emp_id.split('000'))
  let aftersplit_emp_id = e.emp_id.split('000');
  if(aftersplit_emp_id[1])
  {
    console.log('yes');
  }

  if(aftersplit_emp_id[1] && e.employee)
  {
    e.emp_id = e.emp_id.split('000');
    e.emp_id = e.emp_id[1];
  }else if(aftersplit_emp_id[1] && !e.employee){
    document.getElementById('error').innerText="No employee or driver exist with this ID or disabled";
    setTimeout(function(){ document.getElementById('error').innerText=""; },1000);
    return false;
  }
  console.log(e);

if(e.emp_id!=undefined && this.drivertologin!=e.emp_id)
{
  this.drivertologin=e.emp_id;
  this.drivertologin_type = e.employee;
  let end_time = Date.now();
  // console.log(end_time);
console.log(this.loged_in_drivers);
console.log("this.loged_in_drivers.findIndex(x => x.emp_auto_id==e.emp_id)",this.loged_in_drivers.findIndex(x => x.emp_auto_id==e.emp_id && x.emp==e.employee && x.logout_time==undefined));


  if(this.loged_in_drivers.findIndex(x => x.emp_auto_id==e.emp_id && x.emp==e.employee && x.logout_time==undefined)==-1)
  {
    let url = 'http://13.56.147.24:9030/getdriver_by_auto_id/' + e.emp_id + '/nokey';
if(e.employee)
{
   url = 'http://13.56.147.24:9030/getemployee_by_autoid/' + e.emp_id + '/nokey';
}
  console.log(url);
  this.get(url).map((res: Response) => res.json()).subscribe(data => {
    if((<any>data).length==0)
    {
      document.getElementById('error').innerText="No employee or driver exist with this ID or disabled";
        setTimeout(function(){ document.getElementById('error').innerText=""; },1000);
        this.drivertologin=undefined;
        this.drivertologin_type=undefined;
        return false;
    }
    this.data2=data;
    this.http.post('http://13.56.147.24:9030/create-emp_session/nokey', {'emp_id':data[0]['_id'],'emp_name':data[0]['name'],'emp_auto_id':data[0]['auto_id'],'login_time':end_time,type:"driver",'emp':e.employee}).map((res: Response) => res.json()).subscribe(logindata=>{
      console.log('logindata',logindata);
      if(!e.employee)
      {
        (<any>logindata).emp = false;
      }
      
      (<any>logindata).assigned_order=data[0]['assigned_order'];
      document.getElementById('closebutton').click();
//      console.log(JSON.parse(data));
      this.loged_in_drivers.push(logindata);
      sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
      console.log(this.loged_in_drivers);
      this.drivertologin=undefined;
      this.drivertologin_type=undefined;
      f.reset();
   });
  });

}else{
  let driver_index = this.loged_in_drivers.findIndex(x => x.emp_auto_id==e.emp_id && x.emp==e.employee && x.logout_time==undefined);
  console.log('in else',driver_index);
  if(this.loged_in_drivers[driver_index].logout_time!=undefined)
  {
    this.loged_in_drivers.splice(driver_index,1);
    sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
    this.drivertologin=undefined;
    this.drivertologin_type=undefined;
    this.showDriver(e);
  }else{
    this.logout=true;
    this.drivertologin=undefined;
    this.drivertologin_type=undefined;
    this.drivertologout={'id':e.emp_id,'emp':e.employee};
  }
  // this.loged_in_drivers=[];
}
}
}

logout_driver(f)
{
  let e = f.value;
  if(e.employee==undefined && e.employee==null)
  {
    e.employee=false;
  }

  if(e.emp_id==undefined)
  {
    f.reset();
  }
  // console.log(this.drivertologin_type, this.drivertologin_type!=e.employee);
  // console.log(this.drivertologin,e.emp_id,this.drivertologin_type);
  // if(e.emp_id.split('000'))
  let aftersplit_emp_id = e.emp_id.split('000');
  if(aftersplit_emp_id[1])
  {
    console.log('yes');
  }

  if(aftersplit_emp_id[1] && e.employee)
  {
    e.emp_id = e.emp_id.split('000');
    e.emp_id = e.emp_id[1];
  }else if(aftersplit_emp_id[1] && !e.employee){
    document.getElementById('error').innerText="No employee or driver exist with this ID";
    setTimeout(function(){ document.getElementById('error').innerText=""; },1000);
    return false;
  }
  console.log(e);

  if(e.emp_id!=undefined && this.drivertologout.id==e.emp_id && this.drivertologout.emp==e.employee){
  let driver_index = this.loged_in_drivers.findIndex(x => x.emp_auto_id==e.emp_id && x.emp==e.employee && x.logout_time==undefined);
  console.log('logoutdriverindex',driver_index);
    let end_time = Date.now();
  this.http.put('http://13.56.147.24:9030/update-emp_session/' + this.loged_in_drivers[driver_index]._id + '/nokey', {'logout_time':end_time,'emp':e.employee,'emp_name':this.loged_in_drivers[driver_index].emp_name,'emp_id':this.loged_in_drivers[driver_index].emp_id,'emp_auto_id':this.loged_in_drivers[driver_index].emp_auto_id}).map((res: Response) => res.json()).subscribe(logindata=>{
    this.loged_in_drivers.splice(driver_index,1);
    // this.loged_in_drivers[driver_index].logout_time=end_time;
    sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
    this.logout=false;
    f.reset();
      document.getElementById('closebutton').click();
  });
}else{
  document.getElementById('error').innerText="Driver changed please try to login first";
  this.logout=false;
  setTimeout(function(){ document.getElementById('error').innerText=""; },1000);
}

}
// selectrow(){
// console.log("hello")
//   $("#data tr").click(function() {
//       $(this).toggleClass("highlight");
//   });


//   var elements= document.getElementsByTagName('td');
// for(var i=0; i<elements.length;i++)
// {
// (elements)[i].addEventListener("click", function(){
//    alert(this.innerHTML);
// });
// }
// console.log(elements);}
orderbundle_arr=[];
orderbundle(order_auto_id){
  // console.log('order_auto_id',order_auto_id);
  this.orderbundle_arr.push(order_auto_id);
  // console.log('this.orderbundle_arr',this.orderbundle_arr);
}
driver_id;

check_assigned_order(order_auto_id){

  if(this.assigned_order_in_driver_table.length>0)
  {
    if(this.assigned_order_in_driver_table.indexOf(order_auto_id)!=-1)
        {
          return false;
        }else{
          return true;
        }
  }else{
    return true;
  }
  // if(this.loged_in_drivers.length>0)
  // {
  //   for(let k=0; k<this.loged_in_drivers.length; k++)
  //  {console.log(order_auto_id);
  //     if(this.loged_in_drivers[k].assigned_order.indexOf(order_auto_id)!=-1)
  //     {
  //       console.log('false');
  //       return false;
  //     }else{
  //       console.log('true2');
  //       return true;
  //     }
  //  }
  // }else{
  //   console.log('true');
  //   return true;
  // }
}

assign(){
  console.log('orderbundle_arr',this.orderbundle_arr);
  console.log('driverid',this.driver_id);
  console.log(this.all_drivers);
  console.log('employee',this.employee);
  console.log(this.loged_in_drivers);
  // this.size=this.orderbundle_arr.length);
  let index_in_all_driver = this.all_drivers.findIndex(x => x.auto_id==this.driver_id && x.emp==this.employee);
  let index_of_driver = this.loged_in_drivers.findIndex(x => x.emp_auto_id==this.driver_id && x.emp==this.employee);
  if(index_in_all_driver!=-1 && index_of_driver!=-1 && this.orderbundle_arr.length>0)
  {
    // console.log('driver ',this.all_drivers[index_of_driver]);
    this.orderbundle_arr_to_udate = this.orderbundle_arr.concat(this.all_drivers[index_in_all_driver].assigned_order)
     this.assigned_order_in_driver_table = this.assigned_order_in_driver_table.concat(this.orderbundle_arr);
     this.all_drivers[index_in_all_driver].assigned_order = this.all_drivers[index_in_all_driver].assigned_order.concat(this.orderbundle_arr);
    //  console.log("this.all_drivers[index_in_all_driver].assigned_order",this.all_drivers[index_in_all_driver].assigned_order);

    //  this.http.put('http://13.56.147.24:9030/update-emp_session/' + this.loged_in_drivers[index_of_driver]._id + '/nokey', {'assigned_order':this.orderbundle_arr_to_udate}).subscribe(data=>{
      //  console.log(data);
      //  this.all_drivers[index_of_driver].assigned_order
    let api='driver_update';
      if(this.loged_in_drivers[index_of_driver].emp)
      {
        api = "emp_update";
      }
      // console.log("this.orderbundle_arr_to_udate",this.orderbundle_arr_to_udate.length);
       this.http.put('http://13.56.147.24:9030/api/'+api+'/'+this.loged_in_drivers[index_of_driver].emp_id + '/nokey', {'assigned_order':this.orderbundle_arr_to_udate}).map((res: Response) => res.json()).subscribe(data=>{
         this.loged_in_drivers[index_of_driver].assigned_order=this.orderbundle_arr_to_udate;
          sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
        //  this.orderbundle_arr=[];
         this.orderbundle_arr_to_udate=[];
         this.driver_id=undefined;

         this.http.put('http://13.56.147.24:9030/update-orders/nokey', {'assigned_order':this.orderbundle_arr,'driver_id':this.loged_in_drivers[index_of_driver].emp_id,'driver_name':this.loged_in_drivers[index_of_driver].emp_name,'driver_auto_id':this.loged_in_drivers[index_of_driver].emp_auto_id,'assign_to_emp':this.loged_in_drivers[index_of_driver].emp}).subscribe(data=>{
        this.orderbundle_arr=[];
        document.getElementById('clear').click();
        console.log(this.orderbundle_arr);
       });

       });

       
      //  this.loged_in_drivers[index_of_driver].assigned_order = this.orderbundle_arr;

      //  this.orderbundle_arr=[];
      //  console.log('this.orderbundle_arr',this.orderbundle_arr);
    // });
  }else{
    document.getElementById('error').innerText="Please select driver and order both";
      setTimeout(function(){ document.getElementById('error').innerText=""; },1000);
  }
}
driver_orders=[];
driver_index;

remove(order_auto_id,driver_index,index_of_order)
{
  this.loged_in_drivers[driver_index].assigned_order.splice(index_of_order,1);
  this.assigned_order_in_driver_table.splice(this.assigned_order_in_driver_table.indexOf(order_auto_id),1);
  this.driver_orders.splice(index_of_order,1);
  let api='driver_update';
      if(this.loged_in_drivers[driver_index].emp)
      {
        api = "emp_update";
      }
  this.http.put('http://13.56.147.24:9030/api/'+api+'/'+this.loged_in_drivers[driver_index].emp_id + '/nokey', {'assigned_order':this.loged_in_drivers[driver_index].assigned_order}).subscribe(data=>{
    console.log('http://13.56.147.24:9030/update-orders-unassign-driver/nokey');
    this.http.put('http://13.56.147.24:9030/update-orders-unassign-driver/nokey', {'order_auto_id':order_auto_id}).map((res: Response) => res.json()).subscribe(data=>{
      let index = this.data.findIndex(x=>x.auto_id==order_auto_id);
      this.data[index].driverauto_id=-1;
    }); 
    if(this.driver_orders.length==0)
    {
      document.getElementById('close1').click();
    }
    sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
  });
  // sessionStorage.setItem('loged_in_drivers',JSON.stringify(this.loged_in_drivers));
}

driver_order_detail(i)
{
  this.driver_orders=[];
  this.driver_index=i;
   console.log(this.loged_in_drivers[i].assigned_order);
   if(this.loged_in_drivers[i].assigned_order && this.loged_in_drivers[i].assigned_order!=undefined)
   {
  for(let k=0; k<this.loged_in_drivers[i].assigned_order.length; k++)
 {
   console.log(this.loged_in_drivers[i].assigned_order[k]);
   let order_index = this.data.findIndex(x => x.auto_id==this.loged_in_drivers[i].assigned_order[k]);
   console.log('order_index',order_index);
   if(order_index!=-1)
   {
      this.driver_orders.push(this.data[order_index]);
   }
 }
}
 console.log("this.driver_orders",this.driver_orders);
}

checkselect(order_auto_id)
{
  if(this.orderbundle_arr.indexOf(order_auto_id)!=-1)
  {
    return true;
  }else{
    return false;
  }
}

}

//indexof and findiindex
//localstorage and sessionStorage
