import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableHeaderFixerComponent } from './table-header-fixer.component';

describe('TableHeaderFixerComponent', () => {
  let component: TableHeaderFixerComponent;
  let fixture: ComponentFixture<TableHeaderFixerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableHeaderFixerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableHeaderFixerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
